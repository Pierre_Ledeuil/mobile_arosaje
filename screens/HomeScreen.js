import React, { useState } from 'react';
import { ScrollView, View, Text, Image, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'; // Importer FontAwesome ou une autre bibliothèque d'icônes
import { Ionicons } from '@expo/vector-icons';


const HomeScreen = ({ navigation }) => {
    const [selectedItem, setSelectedItem] = useState(null);
    const [showPage, setShowPage] = useState(false);
    const [searchQuery, setSearchQuery] = useState('');

    const handleSearch = () => {
        // Implement your search logic here
        console.log('Searching for:', searchQuery);
    };

    const handleNavigateToMap = () => {
        navigation.navigate('Map');
    };

    // Vos données de Items et de Page
    const ItemData = [
        {
            id: 1,
            title: 'Cactus',
            location: 'Paris',
            date: '01/02/2024-07/02/2024',
            image: require('../assets/cactus.jpg')
        },
        {
            id: 2,
            title: 'Arbuste',
            location: 'New York',
            date: '02/02/2024-06/02/2024',
            image: require('../assets/arbuste.jpg')
        },
        {
            id: 3,
            title: 'Cactus',
            location: 'Paris',
            date: '01/02/2024-07/02/2024',
            image: require('../assets/cactus.jpg')
        },
        {
            id: 4,
            title: 'Arbuste',
            location: 'New York',
            date: '02/02/2024-06/02/2024',
            image: require('../assets/arbuste.jpg')
        },
        {
            id: 5,
            title: 'Cactus',
            location: 'Paris',
            date: '01/02/2024-07/02/2024',
            image: require('../assets/cactus.jpg')
        },
        {
            id: 6,
            title: 'Arbuste',
            location: 'New York',
            date: '02/02/2024-06/02/2024',
            image: require('../assets/arbuste.jpg')
        },
        {
            id: 7,
            title: 'Cactus',
            location: 'Paris',
            date: '01/02/2024-07/02/2024',
            image: require('../assets/cactus.jpg')
        },
        {
            id: 8,
            title: 'Arbuste',
            location: 'New York',
            date: '02/02/2024-06/02/2024',
            image: require('../assets/arbuste.jpg')
        },
        {
            id: 9,
            title: 'Cactus',
            location: 'Paris',
            date: '01/02/2024-07/02/2024',
            image: require('../assets/cactus.jpg')
        },
        {
            id: 10,
            title: 'Arbuste',
            location: 'New York',
            date: '02/02/2024-06/02/2024',
            image: require('../assets/arbuste.jpg')
        }
    ];
    const PageData = [
        {
            id: 1,
            title: 'Cactus',
            location: 'Paris',
            date: '01/02/2024-07/02/2024',
            image: require('../assets/cactus.jpg')
        },
        {
            id: 2,
            title: 'Arbuste',
            location: 'New York',
            date: '02/02/2024-06/02/2024',
            image: require('../assets/arbuste.jpg')
        },
        {
            id: 3,
            title: 'Cactus',
            location: 'Paris',
            date: '01/02/2024-07/02/2024',
            image: require('../assets/cactus.jpg')
        },
        {
            id: 4,
            title: 'Arbuste',
            location: 'New York',
            date: '02/02/2024-06/02/2024',
            image: require('../assets/arbuste.jpg')
        },
        {
            id: 5,
            title: 'Cactus',
            location: 'Paris',
            date: '01/02/2024-07/02/2024',
            image: require('../assets/cactus.jpg')
        },
        {
            id: 6,
            title: 'Arbuste',
            location: 'New York',
            date: '02/02/2024-06/02/2024',
            image: require('../assets/arbuste.jpg')
        },
        {
            id: 7,
            title: 'Cactus',
            location: 'Paris',
            date: '01/02/2024-07/02/2024',
            image: require('../assets/cactus.jpg')
        },
        {
            id: 8,
            title: 'Arbuste',
            location: 'New York',
            date: '02/02/2024-06/02/2024',
            image: require('../assets/arbuste.jpg')
        },
        {
            id: 9,
            title: 'Cactus',
            location: 'Paris',
            date: '01/02/2024-07/02/2024',
            image: require('../assets/cactus.jpg')
        },
        {
            id: 10,
            title: 'Arbuste',
            location: 'New York',
            date: '02/02/2024-06/02/2024',
            image: require('../assets/arbuste.jpg')
        }
    ];

    const handleBlockPress = (ItemId) => {
        setSelectedItem(ItemId);
        setShowPage(true);
    };

    const handleBackToItems = () => {
        setShowPage(false);
    };

    const renderItemBlocks = () => {
        
        

    return (
        <View style={styles.container}>
            <View style={styles.searchContainer}>
                <TextInput
                    style={styles.searchInput}
                    placeholder="Rechercher..."
                    value={searchQuery}
                    onChangeText={text => setSearchQuery(text)}
                    onSubmitEditing={handleSearch}
                />
                <TouchableOpacity style={styles.searchButton} onPress={handleSearch}>
                    <Icon name="search" size={20} color="black" />
                </TouchableOpacity>
            </View>
            <View style={[styles.searchContainer]}>
                <TouchableOpacity onPress={handleNavigateToMap}>
                    <Text style={styles.buttonText}>Aller vers la carte</Text>
                </TouchableOpacity>
            </View>
            <ScrollView
                    >
                {ItemData.map(Item => (
                <TouchableOpacity key={Item.id} onPress={() => handleBlockPress(Item.id)}>
                    <View style={[styles.block]}>
                        <Image
                            style={styles.blockImage}
                            source={Item.image}
                        />
                        <View style={styles.blockText}>
                            <Text style={styles.blockTitle}>{Item.title}</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Icon name="map-marker" size={16} color="#666" style={{ marginRight: 5 }} />
                                <Text style={styles.blockDescription}>{Item.location}</Text>
                            </View>
                            <Text style={styles.blockDescription}>{Item.date}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                            ))}
            </ScrollView>
        </View>
    )};

    const renderPage = () => {
        if (showPage && selectedItem !== null) {
            const selectedPageData = PageData.find(Page => Page.id === selectedItem);
            return (
                <View style={styles.keyboardAvoidingContainer}>
                    <View style={[styles.backView, { height: 80 }]}>
                        <TouchableOpacity style={styles.backButton} onPress={handleBackToItems}>
                            <Ionicons name="arrow-back" size={55} color="black" />
                        </TouchableOpacity>
                        <View style={styles.flexRow}>
                            <Text style={styles.PageTitle}>{selectedPageData.title}</Text>
                        </View>
                    </View>
                        
                    <View style={[styles.pageBlock]}>
                        <Image
                            style={styles.pageImage}
                            source={selectedPageData.image}
                        />
                        <View style={styles.blockPage}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Icon style={styles.pageMarker} name="map-marker" size={34} color="#666" />
                                <Text style={styles.pageCity}>{selectedPageData.location}</Text>
                            </View>
                            <Text style={styles.pageDescription}>{selectedPageData.date}</Text>
                        </View>
                    </View>
                            
                </View>
            );
        }
    };

    return (
        
        <View style={[styles.container]}>
            {showPage ? null : (
                    renderItemBlocks()
            )}
            {renderPage()}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#DFF2D8',
    },
    scrollView: {
        flex: 1,
    },
    block: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 20,
        marginVertical: 10,
        padding: 10,
    },
    firstBlock: {
        marginTop: 0, // Ajouter une marge supérieure uniquement au premier bloc
    },
    blockImage: {
        width: 80,
        height: 80,
        borderRadius: 10,
    },
    blockText: {
        flex: 1,
        marginLeft: 20,
    },
    blockPage:  {
        flex: 1,
        alignItems: 'center',
    },
    blockTitle: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    blockDescription: {
        fontSize: 14,
    },
    
    container: {
        flex: 1,
        backgroundColor: '#DFF2D8',
    },
    searchContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 20,
        marginVertical: 20,
        marginHorizontal:10,
    },
    searchInput: {
        flex: 1,
        borderRadius: 20,
        paddingVertical: 18,
        paddingHorizontal: 20,
        marginRight: 5,
        backgroundColor: 'white',
    },
    searchButton: {
        backgroundColor: 'white',
        padding: 18,
        borderRadius: 20,
    },
    block: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 20,
        marginVertical: 10,
        padding: 10,
        marginHorizontal:10,
    },
    firstBlock: {
        marginTop: 0, // Ajouter une marge supérieure uniquement au premier bloc
    },
    blockImage: {
        width: 80,
        height: 80,
        borderRadius: 10,
    },
    blockText: {
        flex: 1,
        marginLeft: 20,
    },
    blockTitle: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    blockDescription: {
        fontSize: 14,
    },
    
    backView: {
        backgroundColor: '#FFFFFF',
        borderBottomWidth: 1,
        borderBottomColor: '#CCCCCC',
    },
    backButton: {
        position: 'absolute',
        padding: 10,
        zIndex: 1,
    },
    flexRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    pageBlock: {
        backgroundColor: 'white', 
    },  
    blockPage: {
        backgroundColor: 'white', 
        textAlign: 'center',
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',  
        borderTopColor: '#CCCCCC',
        borderTopWidth: 1,
    },
    pageImage: {
        width: '100%', // Pour occuper toute la largeur de l'écran
        height: '100%',
        aspectRatio: 16 / 9, // Ratio d'aspect pour maintenir les proportions de l'image
    },
    PageTitle: {
        fontSize: 30,
        fontWeight: 'bold',
    },
    pageCity: {
        fontSize: 24,
        paddingLeft: 10,
        paddingTop: 20,
    },
    pageDescription: {
        fontSize: 24,
        paddingTop: 20,
        paddingBottom: 20,
    },
    pageMarker : {
        paddingTop: 20,
    },
    
});

export default HomeScreen;
