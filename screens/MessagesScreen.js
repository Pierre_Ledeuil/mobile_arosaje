import React, { useContext, useState, useRef, useEffect } from 'react';
import { TouchableOpacity, ScrollView, View, Text, Image, StyleSheet, TextInput, KeyboardAvoidingView, Platform, Keyboard } from 'react-native'; 
import { Ionicons } from '@expo/vector-icons';

import { CountContext } from '../App';

const MessagesScreen = () => {
    
    const { setNavBarVisible } = useContext(CountContext);
    const [selectedMessage, setSelectedMessage] = useState(null);
    const [showConversation, setShowConversation] = useState(false);
    const [inputMessage, setInputMessage] = useState(''); // État pour gérer le texte saisi
    const inputRef = useRef(null); // Référence pour la zone de texte
    const scrollViewRef = useRef(null); // Référence pour ScrollView
    const [keyboardShown, setKeyboardShown] = useState(false);

    useEffect(() => {
        if (showConversation) {
            // Focus sur la zone de texte lorsqu'on entre dans une conversation
            inputRef.current.focus();
            // Faites défiler vers le bas lorsque la conversation est affichée
            scrollViewRef.current.scrollToEnd({ animated: true });
        }
    }, [showConversation]);

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (event) => {
            // Masquer la barre de navigation lorsque le clavier est affiché
            setNavBarVisible(false);
        });

        const keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => {
            // Réafficher la barre de navigation lorsque le clavier est masqué
            setNavBarVisible(true);
        });

        // Nettoyez les écouteurs d'événements du clavier lorsque le composant est démonté
        return () => {
            keyboardDidShowListener.remove();
            keyboardDidHideListener.remove();
        };
    }, []);

    const messageData = [
        {
            id: 1,
            title: 'Sylvain Durif',
            description: 'La plante doit être arrosée 3 fois...',
            image: require('../assets/human.jpg')
        },
        {
            id: 2,
            title: 'Antoine Daniel',
            description: 'Oui j\'ai bien noté, je vais m\'en...',
            image: require('../assets/human2.jpg')
        },
        {
            id: 3,
            title: 'Mia Calissa',
            description: 'Je n\'en ai pas fait beaucoup mais...',
            image: require('../assets/human3.jpg')
        },
        {
            id: 4,
            title: 'Antoine Daniel',
            description: 'Oui j\'ai bien noté, je vais m\'en...',
            image: require('../assets/human2.jpg')
        },
        {
            id: 5,
            title: 'Mia Calissa',
            description: 'Je n\'en ai pas fait beaucoup mais...',
            image: require('../assets/human3.jpg')
        },
        {
            id: 6,
            title: 'Sylvain Durif',
            description: 'La plante doit être arrosée 3 fois...',
            image: require('../assets/human.jpg')
        },
        {
            id: 7,
            title: 'Antoine Daniel',
            description: 'Oui j\'ai bien noté, je vais m\'en...',
            image: require('../assets/human2.jpg')
        },
        {
            id: 8,
            title: 'Mia Calissa',
            description: 'Je n\'en ai pas fait beaucoup mais...',
            image: require('../assets/human3.jpg')
        },
        {
            id: 9,
            title: 'Sylvain Durif',
            description: 'La plante doit être arrosée 3 fois...',
            image: require('../assets/human.jpg')
        },
        {
            id: 10,
            title: 'Antoine Daniel',
            description: 'Oui j\'ai bien noté, je vais m\'en...',
            image: require('../assets/human2.jpg')
        },
    ];

    const conversationData = [
        { id: 1, message: "Salut, comment ça va ?", received: true },
        { id: 2, message: "Qu'as-tu prévu pour ce week-end ?", received: true },
        { id: 3, message: "As-tu entendu parler du dernier événement ?As-tu entendu parler du dernier événement ?As-tu entendu parler du dernier événement ?As-tu entendu parler du dernier événement ?", received: false },
        { id: 4, message: "Que penses-tu de ce nouveau livre ?", received: false },
        { id: 5, message: "Tu as vu la dernière série ?", received: true },
        { id: 6, message: "Bonjour, comment ça va ?", received: true },
        { id: 7, message: "Que fais-tu aujourd'hui ?", received: false },
        { id: 8, message: "As-tu vu le dernier film ?", received: false },
        { id: 9, message: "Quel temps fait-il chez toi ?", received: true },
        { id: 10, message: "Tu as entendu parler de la nouvelle application ?", received: true },
        { id: 11, message: "Salut, comment ça va ?", received: true },
        { id: 12, message: "Qu'as-tu prévu pour ce week-end ?", received: true },
        { id: 13, message: "As-tu entendu parler du dernier événement ?As-tu entendu parler du dernier événement ?As-tu entendu parler du dernier événement ?As-tu entendu parler du dernier événement ?", received: false },
        { id: 14, message: "Que penses-tu de ce nouveau livre ?", received: false },
    ];

    const handleBlockPress = (messageId) => {
        setSelectedMessage(messageId);
        setShowConversation(true);
    };

    const handleBackToMessages = () => {
        setShowConversation(false);
    };

    const handleSendMessage = () => {
        // Logique pour envoyer le message ici
        // Vous pouvez envoyer le message et réinitialiser l'état de la zone de texte
        setInputMessage('');
    };

    const renderMessageBlocks = () => {
        return messageData.map(message => (
            <TouchableOpacity key={message.id} onPress={() => handleBlockPress(message.id)}>
                <View style={[styles.block, styles.firstBlock]}>
                    <Image
                        style={styles.blockImage}
                        source={message.image}
                    />
                    <View style={styles.blockText}>
                        <Text style={styles.blockTitle}>{message.title}</Text>
                        <Text style={styles.blockDescription}>{message.description}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        ));
    };

    const renderConversation = () => {
        if (showConversation && selectedMessage !== null) {
            const selectedMessageData = messageData.find(message => message.id === selectedMessage);
            return (
                <KeyboardAvoidingView style={styles.keyboardAvoidingContainer}>
                    <View style={[styles.backView, { height: 80 }]}>
                        <TouchableOpacity style={styles.backButton} onPress={handleBackToMessages}>
                            <Ionicons name="arrow-back" size={55} color="black" />
                        </TouchableOpacity>
                        <View style={styles.flexRow}>
                            <Image
                                style={styles.conversationImage}
                                source={selectedMessageData.image}
                            />
                            <Text style={styles.conversationTitle}>{selectedMessageData.title}</Text>
                        </View>
                    </View>

                    <ScrollView
                        style={styles.scrollView}
                        ref={scrollViewRef}
                        contentContainerStyle={styles.scrollContent}
                    >
                        {conversationData.map(conversation => (
                            <View key={conversation.id} style={[
                                styles.block, 
                                conversation.received ? styles.receivedMessage : styles.sentMessage
                            ]}>
                                <View style={styles.conversationText}>
                                    <Text style={styles.conversationDescription}>{conversation.message}</Text>
                                </View>
                            </View>
                        ))}
                    </ScrollView>
                    <View style={styles.inputContainer}>
                        <TextInput
                            ref={inputRef}
                            style={styles.input}
                            placeholder="Tapez votre message..."
                            value={inputMessage}
                            onChangeText={setInputMessage}
                        />
                        <TouchableOpacity style={styles.sendButton} onPress={handleSendMessage}>
                            <Ionicons name="send" size={30} color="black" />
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            );
        }
        return null;
    };

    return (
        <View style={[styles.container]}>
            {showConversation ? null : (
                <ScrollView style={styles.scrollView}>
                    {renderMessageBlocks()}
                </ScrollView>
            )}
            {renderConversation()}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#DFF2D8',
    },
    scrollView: {
        flex: 1,
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderTopWidth: 1,
        borderTopColor: '#CCCCCC',
        paddingVertical: 10,
        paddingHorizontal: 20,
    },
    input: {
        flex: 1,
        height: 50,
        backgroundColor: '#F2F2F2',
        borderRadius: 20,
        paddingHorizontal: 10,
    },
    sendButton: {
        marginLeft: 20,
    },
    sendButtonText: {
        color: '#51B272',
        fontWeight: 'bold',
    },
    keyboardAvoidingContainer: {
        flex: 1,
    },
    block: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        padding: 10,
        borderBottomColor: '#717171',
        borderBottomWidth: 2,
    },
    firstBlock: {},
    blockImage: {
        width: 80,
        height: 80,
        borderRadius: 10,
    },
    blockText: {
        flex: 1,
        marginLeft: 20,
    },
    blockTitle: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    blockDescription: {
        fontSize: 16,
    },
    backView: {
        backgroundColor: '#FFFFFF',
        borderBottomWidth: 1,
        borderBottomColor: '#CCCCCC',
    },
    backButton: {
        position: 'absolute',
        padding: 10,
        zIndex: 1,
    },
    flexRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    conversationTitle: {
        fontSize: 24,
        fontWeight: 'bold',
    },
    conversationImage: {
        width: 60, // Ajustez selon vos besoins
        height: 60, // Ajustez selon vos besoins
        borderRadius: 20, // Assurez-vous de définir la moitié de la largeur ou de la hauteur
        marginRight: 20,
    },
    conversationText: {
        flex: 1,
    },
    conversationDescription: {
        fontSize: 16,
        textAlign: 'left',
        margin: 0,
        padding: 5,
    },
    receivedMessage: {
        alignSelf: 'flex-start',
        backgroundColor: '#FFFFFF',
        maxWidth: '80%',
        marginLeft: 5,
        marginVertical: 10,
        borderBottomColor: 'transparent',
        borderBottomWidth: 0,
        borderRadius: 20,
        borderBottomLeftRadius: 0,
        textAlign: 'center',
    },
    sentMessage: {
        alignSelf: 'flex-end',
        backgroundColor: '#51B272',
        maxWidth: '80%',
        marginRight: 5,
        marginVertical: 10,
        borderBottomColor: 'transparent',
        borderBottomWidth: 0,
        borderRadius: 20,
        borderBottomRightRadius: 0,
        textAlign: 'center',
    },
    scrollView: {
        flex: 1,
    },
    scrollContent: {
        flexGrow: 1,
        justifyContent: 'flex-end',
    },
});

export default MessagesScreen;
