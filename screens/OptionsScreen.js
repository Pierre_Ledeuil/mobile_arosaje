import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { Ionicons } from '@expo/vector-icons'; // Importer Ionicons depuis @expo/vector-icons

const OptionsScreen = () => {
    const [selectedOption, setSelectedOption] = useState([]);
    const [subOptions, setSubOptions] = useState([]);
    const [subSubOptions, setSubSubOptions] = useState([]);
    const [showBackButton, setShowBackButton] = useState(false);

    const handleOptionPress = (option) => {
        setShowBackButton(true); // Afficher le bouton de retour
        // Simuler le chargement des sous-options en fonction de l'option sélectionnée
        if (option === 'Compte') {
            setSubOptions(['Sécurité', 'Informations']);
        } else if (option === 'Notifications') {
            setSubOptions(['Push', 'Email']);
        } else if (option === 'Confidentialité') {
            setSubOptions(['Paramètres', 'Données']);
        } else if (option === 'Sécurité') {
            setSubOptions(['Mot de passe', 'Téléphone']);
        }
    };

    const handleBackButtonPress = () => {
        setShowBackButton(false); // Cacher le bouton de retour
        setSubOptions([]); // Effacer les sous-options affichées
    };

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                {showBackButton && ( // Afficher le bouton de retour s'il est visible
                    <TouchableOpacity style={styles.backButton} onPress={handleBackButtonPress}>
                        <Ionicons name="arrow-back" size={32} color="black" />
                    </TouchableOpacity>
                )}
                <Text style={styles.headerText}>Paramètres</Text>
            </View>
            <View style={styles.optionsContainer}>
                {subOptions.length === 0 && ( // Afficher les options principales si aucune sous-option n'est sélectionnée
                    <>
                        <TouchableOpacity
                            style={[styles.optionButton, selectedOption === 'Compte' && styles.selectedOption]}
                            onPress={() => handleOptionPress('Compte')}>
                            <Text style={styles.optionText}>Compte</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.optionButton, selectedOption === 'Notifications' && styles.selectedOption]}
                            onPress={() => handleOptionPress('Notifications')}>
                            <Text style={styles.optionText}>Notifications</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.optionButton, selectedOption === 'Confidentialité' && styles.selectedOption]}
                            onPress={() => handleOptionPress('Confidentialité')}>
                            <Text style={styles.optionText}>Confidentialité</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.optionButton, selectedOption === 'Sécurité' && styles.selectedOption]}
                            onPress={() => handleOptionPress('Sécurité')}>
                            <Text style={styles.optionText}>Sécurité</Text>
                        </TouchableOpacity>
                    </>
                )}
                {subOptions.map((subOption, index) => (
                    <TouchableOpacity
                        key={index}
                        style={styles.subOptionButton}
                        onPress={() => console.log('Clic')}>
                        <Text style={styles.subOptionText}>{subOption}</Text>
                    </TouchableOpacity>
                ))}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#DFF2D8',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: '#FFFFFF',
        height: 80,
        borderBottomWidth: 2,
        borderBottomColor: '#717171',
        paddingLeft: 20,
    },
    headerText: {
        fontSize: 22,
        fontWeight: 'bold',
        marginRight: 'auto',
    },
    backButton: {
        marginRight: 10,
    },
    optionsContainer: {
        marginBottom: 20,
    },
    optionButton: {
        height: 80,
        justifyContent: 'center',
        alignContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#717171',
        backgroundColor: 'white',
    },
    selectedOption: {
        backgroundColor: '#51B272',
    },
    optionText: {
        fontSize: 20,
        paddingLeft: 20,
    },
    subOptionButton: {
        height: 80,
        justifyContent: 'center',
        alignContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#717171',
        backgroundColor: 'white',
    },
    subOptionText: {
        fontSize: 20,
        paddingLeft: 20,
    },
});

export default OptionsScreen;
