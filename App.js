import React, { useState, createContext, useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { View, StyleSheet, SafeAreaView, StatusBar, Button, TextInput, Text, TouchableOpacity, Keyboard, Image } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import HomeScreen from './screens/HomeScreen';
import MessagesScreen from './screens/MessagesScreen';
import ProfileScreen from './screens/ProfileScreen';
import OptionsScreen from './screens/OptionsScreen';
import MapScreen from './screens/MapScreen'; // Importez MapScreen

const Tab = createBottomTabNavigator();

export const CountContext = createContext();

const LoginScreen = ({ navigation }) => {
  const [showLoginForm, setShowLoginForm] = useState(true);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [fullName, setFullName] = useState('');
  const [error, setError] = useState('');

  const handleLogin = () => {
    if (username === '' && password === '') {
      navigation.navigate('App');
    } else {
      setError('Nom d\'utilisateur ou mot de passe incorrect');
    }
  };

  const handleSignup = () => {
  };

  const switchToSignupForm = () => {
    setShowLoginForm(false);
  };

  const switchToLoginForm = () => {
    setShowLoginForm(true);
  };

  const handleLoginSubmit = () => {
    handleLogin();
    Keyboard.dismiss();
  };

  const handleSignupSubmit = () => {
    handleSignup();
    Keyboard.dismiss();
  };

  return (
    <SafeAreaView style={styles.authContainer}>
      <View style={styles.logoContainer}>
        <Image source={require('./assets/logo.png')} style={styles.logo} resizeMode="contain" />
      </View>
      <View style={styles.formContainer}>
        {showLoginForm ? (
          <>
            <Text style={styles.text}>Connexion</Text>
            <TextInput
              style={styles.input}
              placeholder="Nom d'utilisateur"
              value={username}
              onChangeText={setUsername}
              onSubmitEditing={handleLoginSubmit}
            />
            <TextInput
              style={styles.input}
              placeholder="Mot de passe"
              value={password}
              onChangeText={setPassword}
              secureTextEntry={true}
              onSubmitEditing={handleLoginSubmit}
            />
            <TouchableOpacity style={styles.button} onPress={handleLogin}>
              <Text style={styles.button}>Se connecter</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={switchToSignupForm}>
              <Text style={styles.buttonText}>Pas de compte ? <Text style={styles.underline}>S'inscrire</Text></Text>
            </TouchableOpacity>
          </>
        ) : (
          <>
            <Text style={styles.text}>Inscription</Text>
            <TextInput
              style={styles.input}
              placeholder="Nom complet"
              value={fullName}
              onChangeText={setFullName}
              onSubmitEditing={handleSignupSubmit}
            />
            <TextInput
              style={styles.input}
              placeholder="Email"
              value={email}
              onChangeText={setEmail}
              onSubmitEditing={handleSignupSubmit}
            />
            <TextInput
              style={styles.input}
              placeholder="Nom d'utilisateur"
              value={username}
              onChangeText={setUsername}
              onSubmitEditing={handleSignupSubmit}
            />
            <TextInput
              style={styles.input}
              placeholder="Mot de passe"
              value={password}
              onChangeText={setPassword}
              secureTextEntry={true}
              onSubmitEditing={handleSignupSubmit}
            />
            <TouchableOpacity style={styles.button} onPress={handleSignup}>
              <Text style={styles.button}>S'inscrire</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={switchToLoginForm}>
              <Text style={styles.buttonText}>Déjà un compte ? <Text style={styles.underline}>Se connecter</Text></Text>
            </TouchableOpacity>
          </>
        )}
        {error ? <Text style={styles.error}>{error}</Text> : null}
      </View>
    </SafeAreaView>
  );
};

const App = () => {
  const [count, setCount] = useState(0);
  const [navBarVisible, setNavBarVisible] = useState(true);

  return (
    <CountContext.Provider value={{ count, setCount, navBarVisible, setNavBarVisible }}>
      <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor="#51B272"/>
        <NavigationContainer>
          <Tab.Navigator
            screenOptions={({ route }) => ({
              headerShown: false,
              tabBarStyle: {
                display: navBarVisible ? 'flex' : 'none',
                backgroundColor: '#51B272',
                borderWidth: 0,
                border: 'transparent',
                height: 80,
              },
              tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'Home') {
                  iconName = focused ? 'home' : 'home-outline';
                } else if (route.name === 'Messages') {
                  iconName = focused ? 'chatbox' : 'chatbox-outline';
                } else if (route.name === 'Profile') {
                  iconName = focused ? 'person' : 'person-outline';
                } else if (route.name === 'Options') {
                  iconName = focused ? 'settings' : 'settings-outline';
                }

                return <Ionicons name={iconName} size={40} color={color} />;
              },
              tabBarLabel: () => null
            })}
            tabBarOptions={{
              activeTintColor: 'white',
              inactiveTintColor: 'lightgray',
            }}
          >
            <Tab.Screen name="Home" component={HomeScreen} />
            <Tab.Screen 
              name="Map" 
              component={MapScreen} 
              options={({ route }) => ({
                tabBarIcon: ({ color, size }) => (
                  <Ionicons name="map" size={size} color={color} />
                ) 
              })}
            />
            <Tab.Screen name="Messages" component={MessagesScreen} />
            <Tab.Screen name="Profile" component={ProfileScreen} />
            <Tab.Screen name="Options" component={OptionsScreen} />
            
          </Tab.Navigator>
        </NavigationContainer>
      </SafeAreaView>
    </CountContext.Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    width: '100%',
    marginBottom: 10,
    padding: 10,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
  },
  error: {
    color: 'red',
    marginTop: 10,
  },
  authContainer: {
    flex: 1,
    backgroundColor: '#DFF2D8',
    justifyContent: 'center',
    alignItems: 'center',
  },
  formContainer: {
    backgroundColor: 'white',
    padding: 30,
    borderRadius: 20,
    width: '80%',
  },
  text: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 30,
    marginBottom: 10,
    padding: 10,
    borderRadius: 20,
  },
  input: {
    backgroundColor: '#E5E5E5',
    marginBottom: 20,
    padding: 15,
    borderRadius: 20,
  },
  button: {
    textAlign: 'center',
    color: 'white',
    backgroundColor: '#51B272',
    fontSize: 20,
    padding: 5,
    borderRadius: 20,
  },
  buttonText: {
    textAlign: 'center',
    fontSize: 15,
    marginBottom: 10,
    padding: 10,
    borderRadius: 20,
  },
  underline: {
    textDecorationLine: 'underline',
  },  
  error: {
    color: 'red',
    marginTop: 10,
  },
  logoContainer: {
    top: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    marginTop: -20, // Ajustez cette valeur selon vos besoins
    marginBottom: 40,
  },
  logo: {
    borderRadius: 20,
    width: 150,
    height: 150,
  },
});

export default function MainApp() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  return (
    isLoggedIn ? <App /> : <LoginScreen navigation={{ navigate: setIsLoggedIn }} />
  );
}
